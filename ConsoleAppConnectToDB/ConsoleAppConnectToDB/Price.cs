﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppConnectToDB
{
    class Price
    {
        public DateTimeOffset PublishedPriceDate { get; set; }
        public string MarketName { get; set; }
        public decimal LowPrice { get; set; }
        public decimal MidPrice { get; set; }
        public decimal HighPrice { get; set; }
        public int MarketId { get; set; }

        public Price(SqlDataReader dataReader)
        {
            MarketName = GetMarketName(dataReader);
            LowPrice = GetLowPrice(dataReader);
            HighPrice = GetHighPrice(dataReader);
            MidPrice = (HighPrice + LowPrice) / 2;
            MarketId = GetMarketID(dataReader);
            PublishedPriceDate = GetPublishedPriceDate(dataReader);
        }

        public Price()
        {

        }

        public decimal GetLowPrice(SqlDataReader dataReader)
        {

            int columnNumber = dataReader.GetOrdinal("Low");
            return dataReader.GetDecimal(columnNumber);
        }

        public decimal GetHighPrice(SqlDataReader dataReader)
        {
            int columnNumber = dataReader.GetOrdinal("High");
            decimal highPrice;
            return highPrice = dataReader.GetDecimal(columnNumber);
        }

        public string GetMarketName(SqlDataReader dataReader)
        {
            int columnNumber = dataReader.GetOrdinal("MarketLabelWeb");
            return dataReader.GetString(columnNumber);
        }

        public int GetMarketID(SqlDataReader dataReader)
        {
            int columnNumber = dataReader.GetOrdinal("MarketId");
            return dataReader.GetInt32(columnNumber);
        }

        public DateTimeOffset GetPublishedPriceDate (SqlDataReader dataReader)
        {
            int columnNumber = dataReader.GetOrdinal("PricePublishDate");
            return dataReader.GetDateTimeOffset(columnNumber);
        }

        public override string ToString()
        {
            return MarketName + " " + LowPrice.ToString() + " " + MidPrice.ToString() + " " + HighPrice.ToString() + " " + MarketId.ToString();
        }




    }
}
