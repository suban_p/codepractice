﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppConnectToDB
{
    class Database
    {
        string marketIds;

        public string MarketIds { get; set; }
        public int NumberOfPrices { get; set; }

        public Database()
        {
            Console.WriteLine("Please enter a list of market id's separated by a ','");
            MarketIds = Console.ReadLine();
            Console.WriteLine("Please enter the number of Prices you want");
            NumberOfPrices = Convert.ToInt32(Console.ReadLine());
        }

        public string PublishedPricesQueryBuilder()
        {
            string query = "Select top ";
            query += NumberOfPrices;
            query += " p.PricePublishDate, m.MarketLabelWeb, m.MarketId, pll.PriceLineValue as Low, Coalesce(pll.pricelinevalue,plh.PriceLineValue) as High, p.PriceRevision " +
                " from price p " +
                "join market m on m.marketid = p.marketid " +
                "join priceline pll on pll.priceid = p.priceid and pll.pricelinetypeid = 2 " +
                "join priceline plh on plh.priceid = p.priceid and plh.PriceLineTypeId = 1 " +
                "where p.PriceCalculationTypeId is null " +
                "and p.marketid in (" + MarketIds +
                ")order by p.marketid asc, p.PricePublishDate asc";

            return query;
        }
    }
}
