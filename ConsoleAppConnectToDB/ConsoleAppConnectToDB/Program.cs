﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleAppConnectToDB
{
    class Program
    {
        static void Main(string[] args)
        {
            DatabaseConnectTest();
            //IOPractice();
        }
        public static void DatabaseConnectTest()
        {
            Price price1;
            List<Price> prices = new List<Price>();
            Database database = new Database();
            string connectionStringFilePath = "C:\\Users\\suban\\OneDrive - Euromoney Institutional Investor PLC\\CodePractive\\MindDbConnectionString.txt";
            string mindDBConnectionString;

            StreamReader mindDBConnectionStringReader = new StreamReader(connectionStringFilePath);
            Console.SetIn(mindDBConnectionStringReader);
            mindDBConnectionString = Console.ReadLine();
            mindDBConnectionStringReader.Close();
            

            SqlConnection sqlcon = new SqlConnection(@mindDBConnectionString);

            
            SqlCommand sqlCommand = new SqlCommand(database.PublishedPricesQueryBuilder(), sqlcon);

            sqlcon.Open();
            Console.WriteLine("sqlconnection opened");
            sqlCommand.CommandTimeout = 200;
            SqlDataReader dataReader = sqlCommand.ExecuteReader();

            while (dataReader.Read())
            {
                price1 = new Price(dataReader);
                prices.Add(price1);
            }

            //foreach (Price price in prices)
            //{
            //    Console.WriteLine(price.ToString());
            //}
            string currentDirectory = Directory.GetCurrentDirectory();
            DirectoryInfo directoryInfo = new DirectoryInfo(currentDirectory);

            List<string> fileInfos = new List<string>();

            foreach (FileInfo file in directoryInfo.GetFiles("Data.txt"))
            {
                Console.WriteLine(file.Name);
                fileInfos.Add(file.FullName);
            }
            Console.WriteLine(fileInfos[0]);
            StreamWriter streamWriter = new StreamWriter(fileInfos[0]);
            Console.SetOut(streamWriter);
            foreach (Price price in prices)
            {
                Console.WriteLine(price.ToString());
            }

            try
            {
                streamWriter.Close();
                sqlcon.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public static void IOPractice()
        {

        }
    }
}
