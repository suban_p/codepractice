﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace IOPractice
{
    public class FixMessageCollection
    {
        public List<string> AllFixMessages { get; set; }
        public FixMessageCollection()
        {
            AllFixMessages = new List<string>();
        }

        public void AddFixMessagesToList (StreamReader reader)
        {
            while(reader.Peek() > -1)
            {
                AllFixMessages.Add(reader.ReadLine());
            }
        }

        public void PrintFixCollectionToConsole()
        {
            foreach(string fixMessage in AllFixMessages)
            {
                Console.WriteLine(fixMessage);
            }
        }
    }
}
