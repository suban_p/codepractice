﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace IOPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            FixMessageCollection fixMessageCollection = new FixMessageCollection();
            SingleFixMessage singleFixMessage = new SingleFixMessage(fixMessageCollection);
            string directoryLocation = Directory.GetCurrentDirectory();
            DirectoryInfo directoryInfo = new DirectoryInfo(directoryLocation);
            string file = Path.Combine(directoryInfo.FullName, "data.txt");
            FileInfo fileInfo = new FileInfo(file);
            if (fileInfo.Exists)
            {
                StreamReader fixStream = new StreamReader(fileInfo.FullName);
                fixMessageCollection.AddFixMessagesToList(fixStream);
                fixMessageCollection.PrintFixCollectionToConsole();
            }
            singleFixMessage.FixFieldValueCollection(2, singleFixMessage.FixMessageCollection);
            singleFixMessage.PrintList(singleFixMessage.FieldValueCollection);
            singleFixMessage.FieldValueSplit();
            singleFixMessage.PrintList(singleFixMessage.Fields);
            singleFixMessage.PrintList(singleFixMessage.Values);
            //Console.BackgroundColor = ConsoleColor.DarkYellow;
            //foreach (FileInfo file in files)
            //{
            //    Console.WriteLine(file.Name);
            //}
        }
    }
}
