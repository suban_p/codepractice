﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOPractice
{
    public class SingleFixMessage
    {
        public FixMessageCollection FixMessageCollection;
        public List<string> FieldValueCollection;
        public List<string> Fields { get; set; }
        public List<string> Values { get; set; }
        string fieldValuePairSeparator = "[SOH]";
        string fieldValueSeparator = "=";

        public SingleFixMessage(FixMessageCollection fixMessageCollection)
        {
            FixMessageCollection = fixMessageCollection;
            Fields = new List<string>();
            Values = new List<string>();
            FieldValueCollection = new List<string>();
        }

        public void FixFieldValueCollection (int fixFromCollection, FixMessageCollection fixMessageCollection)
        {
            int startIndex = 0;
            string singleFixMessage = fixMessageCollection.AllFixMessages[fixFromCollection];

            while (startIndex < singleFixMessage.Length)
            {
                int separatorIndex = singleFixMessage.IndexOf(fieldValuePairSeparator, startIndex);
                string FieldValue = singleFixMessage.Substring(startIndex, separatorIndex - startIndex);
                FieldValueCollection.Add(FieldValue);
                startIndex = separatorIndex + fieldValuePairSeparator.Length;
            }
        }

        public void FieldValueSplit()
        {
            int startIndex = 0;
            foreach(string fieldValue in FieldValueCollection)
            {
                int separatorIndex = fieldValue.IndexOf(fieldValueSeparator, startIndex);
                Fields.Add(fieldValue.Substring(startIndex, separatorIndex - startIndex));
                separatorIndex += 1;
                Values.Add(fieldValue.Substring(separatorIndex, fieldValue.Length - separatorIndex));
            }
        }

        public void PrintList(List<string>stringList)
        {
            foreach (string entry in stringList )
            {
                Console.WriteLine(entry);
            }
        }

    }
}
